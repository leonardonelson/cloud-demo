package com.example.demo.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.domain.Product;

@Service
public class ProductService {

	private List<Product> products;

	public ProductService() {
		super();
		products = new LinkedList<>();
		products.add(new Product(111L, "Monster Energetico"));
		products.add(new Product(112L, "Playstation 4"));
		products.add(new Product(113L, "Cha Mate Leao"));
	}

	public List<Product> getProducts() {
		return products;
	}
	
	public Optional<Product> getProduct(Long id) {
		return this.products.stream().filter(product -> product.getId().equals(id)).findFirst();
	}
	
}
