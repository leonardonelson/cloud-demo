package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Cart;
import com.example.demo.domain.CartItem;
import com.example.demo.domain.Product;
import com.example.demo.service.ProductService;

@RestController
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
	ProductService productService;
	
	private Map<Long, Cart> cartMap = new HashMap<>();
	
	@GetMapping("/{cartId}")
	public Cart getCart(Long cartId) {
		return cartMap.get(cartId);
	}
	
	@GetMapping("/orders/{id}")
	public String getOrder(@PathVariable String orderId) {
		return orderId;
	}
	
	@PostMapping
	public Cart createCart() {
		Cart cart = new Cart();
		cartMap.put(cart.getId(), cart);
		return cart;
	}
	
	
	@PostMapping("/{cartId}/product")
	public Cart addProductToCart(@PathVariable(value="cartId") Long cartId, @RequestBody CartItem cartItem) {
		Cart cart = this.cartMap.get(cartId);
		Product product = productService.getProduct(cartItem.getProduct().getId()).get();
		CartItem newCartItem = new CartItem(product, cartItem.getQuantity());
		cart.addItem(newCartItem);
		return cart;
	}
	
}
