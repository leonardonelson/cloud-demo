package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartItem {

    private final Product product;
    private final int quantity;

    public CartItem(Product product, int quantity) {
    	super();
    	this.product = product;
    	this.quantity = quantity;
    }
    
	public int getQuantity() {
		return quantity;
	}
	
	public Product getProduct() {
		return product;
	}

}
