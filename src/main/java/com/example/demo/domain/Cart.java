package com.example.demo.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart {

	private Long id;
	private List<CartItem> cartItems;
	
	public Cart() {
		this.id = System.currentTimeMillis();
	}
	

    public void addItem(CartItem cartItem){
        if(cartItems == null){
            cartItems = new ArrayList<>();
        }
        cartItems.add(cartItem);
    }

    public void removeItem(CartItem cartItem){
        cartItems.remove(cartItem);
    }

	public Long getId() {
		return id;
	}


	public List<CartItem> getCartItems() {
		return cartItems;
	}

}
